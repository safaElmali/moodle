package com.example.safa.cumoodle;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

/**
 * Created by Safa on 01.02.2017.
 */

public class ToolboxClass  extends Activity {

    Button b1,b2,b3,b4,b5,b6;

    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toolbox);


        b1=(Button)findViewById(R.id.ModBttn);
        b2=(Button)findViewById(R.id.AssesBttn);
        b3=(Button)findViewById(R.id.TableBttn);
        b4=(Button)findViewById(R.id.EmailBttn);
        b5=(Button)findViewById(R.id.HelpBttn);
        b6=(Button)findViewById(R.id.feedBttn);
        mDrawerList = (ListView) findViewById(R.id.navList);
        addDrawerItems();

        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://cumoodle.coventry.ac.uk/mod/page/view.php?id=757771");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });



    }
    private void addDrawerItems() {
        String[] osArray = {"Home", "My Course", "Events", "My Modules", "Help", "Tools"};
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
    }
}

